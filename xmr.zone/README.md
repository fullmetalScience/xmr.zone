# About

*xmr.zone* is a collection of Sxmo- and Monero-related software and services by [fullmetalScience](//fullmetal.science)


## Announcements and Discussion

* *Lemmy: [!xmrid@monero.town](//monero.town/c/xmrid)*
* *Matrix: [#xmr.id:monero.social](matrix:r/xmr.id%3Amonero.social?action=join&via=monero.social)*
