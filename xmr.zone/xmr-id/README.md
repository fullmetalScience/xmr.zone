# XMR.ID

XMR.ID is an OpenAlias service sparing you the handling of 95+
character-addresses by instead looking up Monero recipients as `<name>.xmr.id`.

_Obtain your XMR ID at [https://xmr.id](https://xmr.id)._
